#ifndef __DBCONEXION
#define __DBCONEXION
#define MAX_REINTENTOS 1
#include <string>
/** 
 * MySQL conectores...
 */
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
/**
 * Librerías propias
 */
#include "../iolib/IOLogger.h"

using namespace std;
using namespace sql;
using namespace IOCore;

class DBConexion {
public:
	DBConexion(const string svr, const string usr, const string pwd, const string dbnom);
	~DBConexion();
	int ejecutaSQL(string qry, unsigned reintentos = 0);
	int ejecutaBinario(string qry, const unsigned char * b, const size_t pos);
	ResultSet * select(string qry, unsigned reintentos = 0);
// 	void setLogger(IOLogger * elLog);
	bool conectado;
	void termina();
	string ultimoError();
	static string quitaFeos(const string consulta);
	
	string getServidor();
	string getUsuario();
	string getPwd();
	string getDBNombre();
	
private:
	//funciones
	bool conectaDB();
	
	//Variables
	Connection	* dbcon;
	Statement	* stmt;
	
	string		servidor;
	string		usuario;
	string		dbpwd;
	string		dbnombre;
	string		ultError;
};

#endif
