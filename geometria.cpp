#include "geometria.h"

using namespace Geometria;

Geometria::Punto::Punto(double lat, double lng) {
	this->lat = lat;
	this->lng = lng;
}

bool Geometria::Punto::compara(Geometria::Punto * otroPto) {
	return (this->lat == otroPto->getLat() && this->lng == otroPto->getLng());
}

double Geometria::Punto::distancia(Geometria::Punto* otroPto) {
	double a = 0.0, b = 0.0, h = 0.0;
	a = abs(otroPto->getLat() - this->lat);
	b = abs(otroPto->getLng() - this->lng);
	h = sqrt(pow(a, 2) + pow(b, 2));
	return RADIO_TERRESTRE * (2 * M_PI * (h / 360));
}

void Geometria::Punto::desplazaSO(double difLat, double difLng) {
	this->lat -= difLat;
	this->lng -= difLng;
}

void Geometria::Punto::desplazaNE(double difLat, double difLng) {
	this->lat += difLat;
	this->lng += difLng;
}

void Geometria::Punto::promedia(Geometria::Punto* otroPto) {
	this->lat = (this->lat + otroPto->getLat()) / 2;
	this->lng = (this->lng + otroPto->getLng()) / 2;
}

bool Geometria::Punto::valido() {
	return (abs(this->lat) > 0 && abs(this->lat) <= 90) && (abs(this->lng) > 0 && abs(this->lng) < 180);
}

double Geometria::Punto::getLat() {
	return this->lat;
}

double Geometria::Punto::getLng() {
	return this->lng;
}

Geometria::Punto * Geometria::Punto::clona() {
	return new Geometria::Punto(this->lat, this->lng);
}

std::__cxx11::string Geometria::Punto::toString() {
	stringstream c;
	c << setprecision(15) << "lat: " << this->lat << ", lng: " << this->lng;
	return c.str();
}

Geometria::Poligono::Poligono(vector<Geometria::Punto*>* puntos) {
	//Aquí vamos a meter un filtro para no tener puntos repetidos...
	this->coordenadas = new vector<Geometria::Punto*>();
	for (unsigned i = 0; i < puntos->size(); i++) {
		if (!this->contiene(puntos->at(i))) this->coordenadas->push_back(puntos->at(i));
	}
	if (this->coordenadas->size() <= 1) {
		this->coordenadas = 0L;
	}
}

bool Geometria::Poligono::contiene(Geometria::Punto* otroPto) {
	for (Punto * p : *this->coordenadas) {
		if (p->compara(otroPto)) return true;
	}
	return false;
}

bool Geometria::Poligono::dentro(Geometria::Punto* otroPto) {
// 	bool retval = false;
	unsigned i = 0;
	Punto * origen;
	unsigned cuenta = 0;
	
	while (i < this->coordenadas->size()) {
		if (i == this->coordenadas->size() - 1) origen = this->coordenadas->at(0);
		else origen = this->coordenadas->at(i + 1);
		if (this->rayoCortado(otroPto, this->coordenadas->at(i), origen)) cuenta++;
		i++;
	}
	return (cuenta % 2 != 0);
}

bool Geometria::Poligono::rayoCortado(Geometria::Punto* pto, Geometria::Punto* origen, Geometria::Punto* destino) {
// 	bool retval = false, rInf = false, aInf = false;
	Punto * punto = 0L;
	Punto * arriba = 0L;
	Punto * abajo = 0L;
	double rojo, azul;
	
	if (origen->getLat() > destino->getLat()) {
		arriba = origen;
		abajo = destino;
	} else {
		arriba = destino;
		abajo = origen;
	}
	punto = pto->clona();	//Esto es para no alterar el punto original
	if (punto->getLat() == arriba->getLat() || punto->getLat() == abajo->getLat()) punto->desplazaNE(0.000005, 0);
	if ((punto->getLat() < abajo->getLat() || punto->getLat() > arriba->getLat()) || (punto->getLng() > max(arriba->getLng(), abajo->getLng()))) return false;
	if (punto->getLng() < min(arriba->getLng(), abajo->getLng())) return true;
	if (abajo->getLng() != arriba->getLng()) {
		rojo = (arriba->getLat() - abajo->getLat()) / (arriba->getLng() - abajo->getLng());
	} else {
		rojo = 1;
	}
	if (abajo->getLng() != punto->getLng()) {
		azul = (punto->getLat() - abajo->getLat()) / (punto->getLng() - abajo->getLng());
	} else {
		azul = 1;
	}
	delete punto;
	return (azul >= rojo);
}

bool Geometria::Poligono::comparaPuntos(Geometria::Punto* a, Geometria::Punto* b) {
	if (a->getLng() < b->getLng())
		return -1;
	else if (a->getLng() > b->getLng())
		return +1;
	else if (a->getLat() < b->getLat())
		return -1;
	else if (a->getLat() > b->getLat())
		return +1;
	return +1;
}

bool Geometria::Poligono::existeLado(Geometria::Punto* p1, Geometria::Punto* p2) {
	int idx = -1;
	for (int i = 0; i < this->coordenadas->size(); i++) {
		if (this->coordenadas->at(i)->compara(p1)) {
			return p2->compara((i == this->coordenadas->size() - 1 ? this->coordenadas->at(0) : this->coordenadas->at(i + 1)));
		}
	}
	return false;
}
double Geometria::Poligono::area() {
	unsigned lados = this->coordenadas->size() - (this->coordenadas->at(0)->compara(this->coordenadas->at(this->coordenadas->size() - 1)) ? 1 : 0);
	unsigned pos = 0, k = 0, reintenta = 0;
	double retval = 0.0;
	Punto * o = 0L;
	Punto * s = 0L;
	Punto * t = 0L;
	Punto * medio = 0L;
	Triangulo * tri = 0L;
	unsigned triangulos = 0;
	vector<Punto*> pol(*this->coordenadas);
	//Generamos los triángulos
	while(triangulos < (lados - 2)) {
		if (pos + 2 < pol.size()) {
			//Validamos que se vaya a completar el triángulo.
			o = Poligono::sigPunto(&pol, &pos);
			s = Poligono::sigPunto(&pol, &pos);
			t = Poligono::sigPunto(&pol, &pos);
			if (pos > 1) pos -= 1;
			//Validar si el punto hace triángulo dentro del polígono.
			medio = new Punto((o->getLat() + t->getLat()) / 2, (o->getLng() + t->getLng()) / 2);
			if (this->dentro(medio) || pol.size() == 3) {
				//Sí, el punto es válido, el triángulo se forma
				pol.erase(pol.begin() + --pos);
				tri = new Triangulo(o, s, t);
				triangulos++;
				retval += tri->area();
				delete tri;
			} else {
				if (pos > 0) pos -= 1;
			}
		} else {
			pos = 0;
		}
		if (k == pol.size()) {
			//Vamos a imprimir los puntos restantes...
			if (++reintenta > this->coordenadas->size()) {
				cerr << "Evita ciclo infinito!\tpos: " << pos << "\tPuntos: " << pol.size() << std::endl;
				break;
			}
			if (pol.size() == 3) pos = 0;
		} else {
			reintenta = 0;
		}
		k = pol.size();
	}
	return retval;
}
//149597870.700
void Geometria::Poligono::trazar() {
	//Envia al std::err un trazo para GNUplot. Solo sirve si está el modoX activo.
	if (this->archGrafica != "") {
		stringstream gr;
		for (unsigned i = 0; i < this->coordenadas->size(); i++) {
			gr << std::setprecision(15) << this->coordenadas->at(i)->getLng() << " " << this->coordenadas->at(i)->getLat() << std::endl;
		}
		gr << this->coordenadas->at(0)->getLng() << " " << this->coordenadas->at(0)->getLat() << std::endl;
		Poligono::exportaCadena(gr.str(), this->archGrafica, 0);
	}
}
void Geometria::Poligono::setArchGrafica(const std::__cxx11::string sale) {
	this->archGrafica = sale;
}

void Geometria::Poligono::exportaCadena(const std::__cxx11::string cadena, const std::__cxx11::string archivo, const unsigned secuencia) {
	stringstream snom;
	snom << archivo << "_" << secuencia << ".dat";
	ofstream salida;
	salida.open(snom.str().c_str(), ofstream::out | ofstream::trunc);
	salida << cadena;
	salida.flush();
	salida.close();
}

Geometria::Punto * Geometria::Poligono::sigPunto(vector<Geometria::Punto *>* lista, unsigned int* laPos) {
	if (*laPos == lista->size()) *laPos = 0;
	return lista->at((*laPos)++);
}

bool Geometria::Poligono::buscaTriangulo(vector<Geometria::Triangulo *>* contenedor, Geometria::Triangulo* tri) {
	for (Triangulo * t : *contenedor) {
		if (tri->compara(t)) return true;
	}
	return false;
}

Geometria::Triangulo::Triangulo(Geometria::Punto* p1, Geometria::Punto* p2, Geometria::Punto* p3) {
	this->puntos[0] = p1;
	this->puntos[1] = p2;
	this->puntos[2] = p3;
	this->ladoA = p1->distancia(p2);
	this->ladoB = p2->distancia(p3);
	this->ladoC = p3->distancia(p1);
}

double Geometria::Triangulo::area() {
	double semiperimetro = (this->ladoA + this->ladoB + this->ladoC) / 2;
	double calculo = semiperimetro * (semiperimetro - this->ladoA ) * (semiperimetro - this->ladoB) * (semiperimetro - this->ladoC);
	return sqrt(calculo);
}

bool Geometria::Triangulo::compara(Geometria::Triangulo* otro) {
	bool retval = true;
	for (unsigned i = 0; i < 3; i++) {
		retval = retval && this->buscaPunto(otro->getPunto(i));
	}
	return retval;
}

Geometria::Punto * Geometria::Triangulo::getPunto(unsigned int pos) {
	if (pos < 3)
		return this->puntos[pos];
	else
		return 0L;
}

bool Geometria::Triangulo::buscaPunto(Geometria::Punto* p) {
	for (unsigned i = 0; i < 3; i++) {
		if (this->puntos[i]->compara(p)) return true;
	}
	return false;
}

std::__cxx11::string Geometria::Triangulo::toString() {
	stringstream salida;
	for (unsigned i = 0; i < 3; i++) {
		salida << i << ": " << this->puntos[i]->toString() << "\t";
	}
	salida << "\n";
	return salida.str();
}

std::__cxx11::string Geometria::Triangulo::toSQL() {
	stringstream salida;
	salida << "POLYGON((";
	for (unsigned i = 0; i < 3; i++) {
		salida << setprecision(15) << this->puntos[i]->getLng() << " " << this->puntos[i]->getLat() << ",";
	}
	salida << this->puntos[0]->getLng() << " " << this->puntos[0]->getLat() << "))";
	return salida.str();
}
