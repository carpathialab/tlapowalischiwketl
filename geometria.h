#ifndef __GEOMETRIA
#define __GEOMETRIA
#define RADIO_TERRESTRE 6379500
#include <math.h>
#include <vector>
#include <bits/stdc++.h>

using namespace std;

namespace Geometria {
	class Punto {
	public:
		Punto(double lat, double lng);
		bool compara(Geometria::Punto * otroPto);
		double distancia(Geometria::Punto * otroPto);
		void desplazaSO(double difLat, double difLng);
		void desplazaNE(double difLat, double difLng);
		void promedia(Geometria::Punto * otroPto);
		bool valido();
		double getLat();
		double getLng();
		Punto* clona();
		string toString();
		
	private:
		double lat, lng;
	};
	class Triangulo {
	public:
		Triangulo(Punto * p1, Punto * p2, Punto * p3);
		double area();
		bool compara(Triangulo * otro);
		Punto * getPunto(unsigned pos);
		string toString();
		string toSQL();
		
	private:
		Punto * puntos[3];
		double ladoA;
		double ladoB;
		double ladoC;

		bool buscaPunto(Punto * p);
	};
	class Poligono {
	public:
		Poligono(vector<Punto*> * puntos);
		bool contiene(Punto * otroPto);
		bool dentro(Punto * otroPto);
		void trazar();
		void setArchGrafica(const string sale);
		double area();

	private:
		string archGrafica;
		vector<Punto*> * coordenadas;
		vector<Triangulo*> triangulos;
		Punto * sigPunto(vector < Punto* > *lista, unsigned *laPos);
		static bool comparaPuntos(Punto * a, Punto * b);
		bool existeLado(Punto * p1, Punto * p2);
		bool rayoCortado(Punto * punto, Punto * origen, Punto * destino);
		static bool buscaTriangulo(vector<Triangulo*> * contenedor, Triangulo * tri);
		static void exportaCadena(const std::__cxx11::string cadena, const std::__cxx11::string archivo, const unsigned secuencia);
	};
}

#endif
