#ifndef __INICIO
#define __INICIO
#include <iostream>
#include <string>
#include "geometria.h"
#include "DBConexion.h"


using namespace std;

DBConexion 		* dbman;
bool			verboso;
string			consulta;
string			param;
string			archGrafica;
vector<string> split(string str, string delim, unsigned max = 0);
double calculaArea ( const std::__cxx11::string qry );
double diferencia();

#endif
