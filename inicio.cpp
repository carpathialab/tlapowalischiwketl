#include "inicio.h"

/**
 * Definición:
 * 
 * Parámetros:
 * Función: 1 para calcular un área
 * ID del polígono
 * 
 * Devuelve el área
 * 
 * 
 *
 */
int main(int argc, char **argv) {
	verboso = false;
	string dbusr = "", dbpwd = "", dbnom = "", dbsvr = "localhost:3306";
	archGrafica = "";
	dbman = 0L;
	
	for (unsigned ar = 1; ar < argc; ar++) {
		switch(argv[ar][1]) {
			case 'v':
				cout << "Activando modo platicador...\n";
				verboso = true;
				break;
			case 'h':
				cout << "Modo de uso: tlapowalischiwketl -a [Consulta SQL que trae uno o dos polígono ASTEXT]\n";
				cout << "\t-v: activa modo expresivo en stdout\n";
				cout << "\t-g [Archivo de salida]: genera datos para GNUplot\n";
				cout << "\t-c [Nombre DB MariaDB]: nombre de la base de datos\n";
				cout << "\t-b [usuario MariaDB]: Usuario para conexión a la base de datos\n";
				cout << "\t-d [Contraseña MariaDB]: Contraseña en la base de datos MariaDB\n";
				cout << "\t-e [Servidor MariaDB]: Servidor de la base de datos, 'localhost:3306' como predeterminado, para especificar un puerto, usar formato host:puerto\n";
				return 2;
				break;
			case 'a':
				consulta = string(argv[++ar]);
				break;
			case 'c':
				dbnom = string(argv[++ar]);
				break;
			case 'g':	//Graficar el polígono
				archGrafica = string(argv[++ar]);
				break;
			case 'b':
				dbusr = string(argv[++ar]);
				break;
			case 'd':
				dbpwd = string(argv[++ar]);
				break;
			case 'e':
				dbsvr = string(argv[++ar]);
				break;
		}
	}
	dbman = new DBConexion(dbsvr, dbusr, dbpwd, dbnom);
	if (!dbman->conectado) {
		cerr << "Error conectando a la base de datos, saliendo\n";
		return 1;
	} else {
		//Generar el manejador de estados
		double res = 0.0;
		res = calculaArea(consulta);
		cout << setprecision(15) << res << endl;
	}
	return 0;
}

double calculaArea (const string qry) {
	//ATENCIÓN: todo debe de estar en una tabla especial.
	string lat = "", lng = "";
	vector<Geometria::Punto* > * ptos = new vector<Geometria::Punto*>();
	ResultSet * rs = dbman->select(qry);
	if (rs != 0L) {
		while (rs->next()) {
			string def = rs->getString(1);
			if (def.substr(0, 9) == "POLYGON((") {
				//Limpiar el texto...
				def = def.substr(9);
				def = def.substr(0, def.length() -2);
				//Generar los puntos...
				vector<string> p = split(def, ",");
				for (unsigned i = 0; i < p.size(); i++) {
					vector<string> c = split(p.at(i), " ");
					ptos->push_back(new Geometria::Punto(std::stod(c.at(1)), std::stod(c.at(0))));
				}
				//Crear el polígono.
				Geometria::Poligono * poli = new Geometria::Poligono(ptos);
				poli->setArchGrafica(archGrafica);
				poli->trazar();
				return poli->area();
			}
		}
	}
	return -1;
}
vector<string> split(string str, string delim, unsigned int max) {
/* Esta función funciona como el split tradicional de java, la hago a mano por que no encuentro nada que funcione como tal... 	*/
	vector<string>	retval;
    unsigned		cnt = 0, pos = 0;
	//Contamos delimitadores...
	if (str == "") 
		return retval;
	
	if (str.substr(str.length() - delim.length(), delim.length()) != delim)
		str += delim;
	
	for (unsigned i = 0; i < str.length(); i++) {
		int pasa = str.find(delim, i);
		if (pasa > 0) {
			i = pasa;
			cnt++;
		}
	}
	if (max != 0 && cnt > max)	cnt = max; 
	
	while (cnt > 0) {
		pos = str.find(delim);
		retval.push_back(str.substr(0, pos));
		str.erase(0, (pos + delim.length()));
		cnt--;
	}
	if (str != "") {
		retval[retval.size() - 1] += delim + str;
	}
	return retval;
}
