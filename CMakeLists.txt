cmake_minimum_required(VERSION 3.0)

project(tlapowalischiwketl)

add_executable(tlapowalischiwketl inicio.cpp geometria.cpp DBConexion.cpp)

target_link_libraries(tlapowalischiwketl mysqlcppconn )

install(TARGETS tlapowalischiwketl RUNTIME DESTINATION bin)
